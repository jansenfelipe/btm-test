<?php

namespace JansenFelipe\BtmTest;

use GuzzleHttp\Client;

class AlbumService
{
    /**
     * @var \GuzzleHttp\Client;
     */
    private $client;

    /**
     * AlbumService constructor.
     */
    function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://jsonplaceholder.typicode.com/',
        ]);
    }

    /**
     * List Albums
     *
     * @param $id
     * @param $data
     * @return array
     */
    public function get()
    {
        $response = $this->client->request('GET', '/albums');

        return json_decode($response->getBody());
    }

    /**
     * Create Album
     *
     * @param $id
     * @param $data
     * @return array
     */
    public function post($data)
    {
        $response = $this->client->request('POST', '/albums', $data);

        return json_decode($response->getBody());
    }

    /**
     * Update Album
     *
     * @param $id
     * @param $data
     * @return array
     */
    public function put($id, $data)
    {
        $response = $this->client->request('PUT', '/albums/' . $id, $data);

        return json_decode($response->getBody());
    }
}