<?php

require_once 'vendor/autoload.php';

$albumService = new \JansenFelipe\BtmTest\AlbumService();

/*
 * Get Albums
 */
echo 'Buscando albums..' . PHP_EOL;

$list = $albumService->get();

echo 'Quantidade de albums encontrada: ' . count($list) . PHP_EOL;

/*
 * Post Albums
 */
echo 'POST Album..' . PHP_EOL;

$result = $albumService->post([
    'userId' => 55,
    'id' => 66,
    'title' => 'Teste BTM'
]);

var_dump($result);

/*
 * Put Albums
 */
echo 'PUT Album..' . PHP_EOL;

$result = $albumService->put(66, [
    'userId' => 33,
    'title' => 'Teste BTM atualizacao'
]);

var_dump($result);