# Test BTM

## Como executar o projeto

Clonar o repositório

```sh
$ git clone https://bitbucket.org/jansenfelipe/btm-test.git
```

Instalar dependencias

```sh
$ composer install
```

Executar arquivo `btm-test.php`

```sh
$ php btm-test.php
```

## Demo

![Demo](https://bytebucket.org/jansenfelipe/btm-test/raw/78747de45c20cdde6a9f349c37a023366212cadf/images/demo.gif?token=5f2958ee6ee0e2c978dfe5887533bd4e0a5bf499 "Demo")